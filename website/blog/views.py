from django.shortcuts import render


def index(request):
    return render('blog/index.html')
