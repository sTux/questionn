# coding=utf-8
from django import forms
from . import models
from pagedown.widgets import PagedownWidget


class QuestionForm(forms.ModelForm):
    title = forms.CharField(max_length=250, label='Enter title for the Question')
    body = forms.CharField(widget=PagedownWidget(), label='Enter the questions details')

    class Meta:
        model = models.Question
        exclude = ('asked_by', 'slug', 'pub_date', 'up_votes', 'down_votes')


class AnswerForm(forms.ModelForm):
    draft = forms.BooleanField(label='Save as draft', required=False, initial=False)
    answer = forms.CharField(widget=PagedownWidget())

    class Meta:
        model = models.Answer
        exclude = ('question', 'published_date', 'answered_by', 'up_votes', 'down_votes')


class CommentsForm(forms.ModelForm):
    class Meta:
        model = models.Comment
        exclude = ('question_relation', 'answer_relation', 'pub_date', 'comment_by', 'up_votes', 'down_votes')
