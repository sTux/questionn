# coding=utf-8
from .helpers import generate_random_id
from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils import timezone

class Question(models.Model):
    title = models.CharField(max_length=250)
    body = models.TextField()
    asked_by = models.ForeignKey("users.UserModel")
    pub_date = models.DateTimeField(auto_now_add=timezone.now)
    slug = models.SlugField(max_length=350, unique=True)
    up_votes = models.PositiveIntegerField(default=0)
    down_votes = models.PositiveIntegerField(default=0)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # super(Question, self).save(force_insert, force_update, using, update_fields)
        if not self.slug:
            self.slug = slugify(self.title)+"-"+generate_random_id()
        super(Question, self).save(force_insert, force_update, using, update_fields)

    def get_absolute_url(self):
        return reverse('question:details', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Answer(models.Model):
    answer = models.TextField()
    question = models.ForeignKey(Question, models.CASCADE)
    published_date = models.DateTimeField(auto_now_add=timezone.now)
    last_edited = models.DateTimeField(auto_now=timezone.now)
    answered_by = models.ForeignKey('users.UserModel', models.CASCADE)
    draft = models.BooleanField(default=False)
    up_votes = models.PositiveIntegerField(default=0)
    down_votes = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.question.title+"===>"+self.answer[:31]


class Comment(models.Model):
    question_relation = models.ForeignKey(Question, models.CASCADE, blank=True, null=True)
    answer_relation = models.ForeignKey(Answer, models.CASCADE, blank=True, null=True)
    body = models.CharField(max_length=300)
    pub_date = models.DateTimeField(auto_now_add=True)
    comment_by = models.ForeignKey('users.UserModel', models.CASCADE)
    up_votes = models.PositiveIntegerField(default=0)
    down_votes = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "{0} by {1} at {2}".format(self.body, self.comment_by, self.pub_date)
