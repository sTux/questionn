# coding=utf-8
from django.views.generic import UpdateView
from Questionnaire import models, forms
from django.urls import reverse


class QuestionUpdateView(UpdateView):
    model = models.Question
    form_class = forms.QuestionForm
    template_name = 'Questionnaire/form.html'

    def get_success_url(self):
        target_slug = self.object.slug
        return reverse('question:details', kwargs={'slug': target_slug})
