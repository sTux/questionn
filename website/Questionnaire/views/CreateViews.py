# coding=utf-8
from .. import models, forms
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import CreateView


class QuestionCreateView(CreateView):
    model = models.Question
    form_class = forms.QuestionForm
    template_name = 'Questionnaire/form.html'
    success_url = 'question:details'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated() == False:
            return redirect('users:login_user')
        return super(QuestionCreateView, self).get(request, args, kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.asked_by = request.user
            model.save()
            return redirect(self.success_url, slug=model.slug)


class AnswerCreateView(CreateView):
    model = models.Answer
    form_class = forms.AnswerForm
    template_name = 'Questionnaire/form.html'
    success_url = 'question:details'

    def post(self, request, *args, **kwargs):
        question = get_object_or_404(models.Question, slug=kwargs['slug'])
        form = self.form_class(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.question = question
            model.answered_by = request.user
            model.save()
            return redirect(self.success_url, slug=kwargs.get('slug'))


class CommentCreateView(CreateView):
    model = models.Comment
    form_class = forms.CommentsForm
    template_name = 'Questionnaire/form.html'
    success_url = 'question:details'

    def post(self, request, *args, **kwargs):
        reference_model = kwargs['refer_model']
        question_model, answer_model = None, None
        if reference_model == 'question':
            question_model = get_object_or_404(models.Question, pk=kwargs['pk'])
        else:
            answer_model = get_object_or_404(models.Answer, pk=kwargs['pk'])
        form = self.form_class(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.question_relation, model.answer_relation = question_model, answer_model
            model.comment_by = request.user
            model.save()
            return redirect(self.success_url, slug=kwargs['question_slug'])
