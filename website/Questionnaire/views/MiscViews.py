# coding=utf-8
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import get_object_or_404, render
from Questionnaire import models
from django.views.generic import ListView, DetailView


class QuestionIndexView(ListView):
    model = models.Question
    context_object_name = 'questions'
    template_name = 'Questionnaire/index.html'
    ordering = '-pub_date'

    def get(self, request, *args, **kwargs):
        pages = Paginator(self.get_queryset(), 10)
        page = request.GET.get('page')

        try:
            questions = pages.page(page)
        except PageNotAnInteger:
            questions = pages.page(1)
        except EmptyPage:
            questions = pages.page(pages.num_pages)

        return render(request, self.template_name, {self.context_object_name: questions})


class QuestionDetailView(DetailView):
    model = models.Question
    context_object_name = 'question'
    template_name = 'Questionnaire/question_details.html'

    def get(self, request, *args, **kwargs):
        question = get_object_or_404(self.model, slug=kwargs['slug'])
        answers = question.answer_set.order_by('-published_date')
        return render(request, self.template_name, {self.context_object_name: question, 'answers': answers})
