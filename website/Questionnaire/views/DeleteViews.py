# coding=utf-8
from django.views.generic import DeleteView
from django.shortcuts import get_object_or_404, redirect
from .. import models


class QuestionDeleteView(DeleteView):
    model = models.Question
    success_url = 'question:index'
    template_name = 'Questionnaire/question_confirm_delete.html'

    def post(self, request, *args, **kwargs):
        question = get_object_or_404(self.model, slug=kwargs['slug'])
        if question.asked_by == request.user:
            question.delete(keep_parents=False)
            return redirect(self.success_url)
        else:
            return redirect('question:details', slug=kwargs['slug'])


class AnswerDeleteView(DeleteView):
    model = models.Answer
    success_url = 'question:details'
    template_name = 'Questionnaire/answer_confirm_delete.html'

    def post(self, request, *args, **kwargs):
        question = get_object_or_404(models.Question, slug=kwargs['slug'])
        answer = get_object_or_404(question.answer_set, pk=kwargs['pk'])
        if request.user == answer.answered_by:
            answer.delete(keep_parents=True)
        return redirect('question:details', slug=kwargs['slug'])


class CommentDeleteView(DeleteView):
    model = models.Comment
    success_url = 'question:details'

    def get(self, request, *args, **kwargs):
        comment = get_object_or_404(self.model, pk=kwargs['pk'])
        if request.user == comment.user:
            comment.delete(keep_parents=True)
        return redirect(self.success_url, slug=kwargs['slug'])
