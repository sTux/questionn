# coding=utf-8
from django.apps import AppConfig
from watson import search


class QuestionarreConfig(AppConfig):
    name = 'Questionnaire'

    def ready(self):
        Model = self.get_model("Question")
        search.register(Model)
