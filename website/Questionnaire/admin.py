from django.contrib import admin
from . import models


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
    list_filter = ['pub_date', ]

@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_filter = ['published_date', 'last_edited']

@admin.register(models.Comment)
class CommentAdmin(admin.ModelAdmin):
    list_filter = ['pub_date', ]
