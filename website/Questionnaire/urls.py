# coding=utf-8
from django.conf.urls import url
from .views import CreateViews, DeleteViews, UpdateViews, MiscViews

app_name = 'question'

urlpatterns = [
    url(r'^$', MiscViews.QuestionIndexView.as_view(), name="index"),
    url(r'^add/$', CreateViews.QuestionCreateView.as_view(), name='add'),
    url(r'^question/(?P<slug>[-\w]+)/$', MiscViews.QuestionDetailView.as_view(), name='details'),
    url(r'^answer/(?P<slug>[-\w]+)/$', CreateViews.AnswerCreateView.as_view(), name='answer'),
    url(r'^comment/(?P<question_slug>[-\w]+)/(?P<pk>[0-9]+)/(?P<refer_model>[\w]+)/$',
        CreateViews.CommentCreateView.as_view(), name='comment'),

    url(r'^answer(?P<slug>[-\w]+)/(?P<pk>[0-9]+)/delete/$',
        DeleteViews.AnswerDeleteView.as_view(), name='answer_delete'),

    url(r'^comment/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/delete',
        DeleteViews.CommentDeleteView.as_view(), name='delete_comment'),

    url(r'^question/(?P<slug>[-\w]+)/delete/$', DeleteViews.QuestionDeleteView.as_view(), name='delete_question'),
    url(r'^question/(?P<slug>[-\w]+)/edit/$', UpdateViews.QuestionUpdateView.as_view(), name='edit_question'),

    # url(r'like/(?P<slug>[-\w]+)/(?P<pk>[-0-9]+)/(?P<refer_model>[-\w]+)/(?P<like>[\w]+)/',
    #    CreateViews.LikeCreateView.as_view(), name='like'),
]
