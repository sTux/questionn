# coding=utf-8
"""
website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^u/', include('users.urls', namespace='users', app_name='users')),
    url(r'^blog/', include('blog.urls', namespace='blog', app_name='blog')),
    url(r'^', include('Questionnaire.urls', namespace="question", app_name="question")),
]
if settings.DEBUG:
    from django.contrib import admin
    import debug_toolbar
    urlpatterns.insert(0, url(r'^admin/', admin.site.urls))
    urlpatterns.insert(0, url(r'^debug/', include(debug_toolbar.urls)))

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
