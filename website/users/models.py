# coding=utf-8
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager


class UserModel(AbstractUser):
    # User's details
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(unique=True, blank=True)
    user_details = models.TextField(max_length=200)
    profile_pictures = models.ImageField(blank=True)

    # House Keeping details
    date_joined = models.DateTimeField(auto_now_add=True)
    is_staff = models.BooleanField(default=False, editable=False)
    is_superuser = models.BooleanField(default=False, editable=False)

    objects = UserManager()

    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    class Meta:
        verbose_name = 'Registered User'
        verbose_name_plural = 'Registered Users'

    def get_short_name(self):
        "Returns the short name for the user."
        return self.username

    def get_full_name(self):
        return "%s %s" %(self.first_name, self.last_name)
