# coding=utf-8
from django.shortcuts import Http404
from django.shortcuts import redirect, render_to_response
from django.contrib.auth import login, logout, decorators, authenticate
from django.views.generic import FormView, DetailView, CreateView
from .forms import UserLoginForm, UserRegisterForm
from .models import UserModel


class UserLoginView(FormView):
    template_name = 'users/login.html'
    model = UserModel
    form_class = UserLoginForm


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            user_name = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=user_name, password=password)
            if user:
                login(request, user)
                return redirect('users:user_details', username=user_name)
            else:
                return self.get(request)

@decorators.login_required
def logout_user(request):
        logout(request)
        return redirect('question:index')


class UserDetailView(DetailView):
    model = UserModel
    template_name = 'users/user_details.html'
    context_object_name = 'user'
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserRegisterView(CreateView):
    model = UserModel
    form_class = UserRegisterForm
    template_name = 'users/user_register.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        print(form.is_valid())
        if form.is_valid():
            user_model = form.save(commit=False)
            print(user_model)
            user_model.password = user_model.set_password(user_model.password)
            user_model.save()
            user_model = authenticate(username=user_model.username, password=user_model.password)
            print(user_model)
            login(request, user=user_model)
            return redirect('users:user_details', username=user_model.username)
        else:
            return Http404(message="The credentials were not valid")
