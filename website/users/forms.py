# coding=utf-8
from django import forms
from django.contrib.auth import get_user_model, login
from .models import UserModel


class UserLoginForm(forms.Form):
    username = forms.CharField()
    # email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())


class UserRegisterForm(forms.ModelForm):
    username = forms.CharField(
        max_length=150,
        label="Enter a username for you account within 150 letters"
    )

    password = forms.CharField(widget=forms.PasswordInput(), label="Create a password")
    password2 = forms.CharField(widget=forms.PasswordInput(), label="Confirm your password")

    email = forms.EmailField(label="Enter a valid e-mail id (optional)", required=False)

    def is_valid(self):
        if self.password != self.password2:
            return False
        return super(UserRegisterForm, self).is_valid()

    class Meta:
        model = UserModel
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
