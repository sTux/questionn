from random import choice, randint
from string import ascii_lowercase


def generate_random_id():
    """Generate a random id portion for the slug"""
    random_choices = ascii_lowercase +"123456789"
    rand_part = ''.join(choice(random_choices) for _ in range(0, randint(0,9)))
    return rand_part
