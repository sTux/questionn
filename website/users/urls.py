# coding=utf-8
from django.conf.urls import url
from .views import UserLoginView, logout_user, UserDetailView, UserRegisterView

urlpatterns = [
    url(r'^login/$', UserLoginView.as_view(), name='login_user'),
    url(r'^logout/$', logout_user, name='logout_user'),
    url(r'^register/$', UserRegisterView.as_view(), name='register_user'),
    url(r'^(?P<username>[-\w]+)/$', UserDetailView.as_view(), name='user_details'),
]
