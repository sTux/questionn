# Contributing to this project
<ol>
<li> All the Python or Django must be formatted to PEP8 guidlines. </li>
<li>All the codes that you have contributed, whether its a template or view or a model, must have a documentaions, in form of comments or docstrings wherever applied</li>
<li> All the variable names must be named in such a mannaer that it should clearly reflect its purpose</li>
</ol>

# Basing of you work on this project.
If you are basing off your project from the codes present in project, then you have to return each and every change back to this project 
and that too, to the respective maintainers 
Be sure to read the [License](LICENSE)